---
title: youtube
description: Микрокод для встраивания виджета видео YouTube
---
Данный микрокод возвращает виджет YouTube-видео на основе его идентификатора.

```markdown
{{</* youtube hfPnq3i4Udw */>}})
```

Результатом работы микрокода станет следующее:

{{< youtube hfPnq3i4Udw >}}
