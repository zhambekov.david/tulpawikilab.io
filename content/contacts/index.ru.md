---
type: contacts
title: Контакты
description: Информация для обратной связи
aliases:
  - /ru/tulpawiki/контакты
contacts:
  - givenName: Максим
    familyName: Лебедев
    image: "toby3d"
    email: toby3d@tulpawiki.org
    jobTitle: руководитель
  - name: Verdana
    jobTitle: редактор
    email: verdana@tulpawiki.org
resources:
  - name: "toby3d"
    src: toby3d.jpg
    title: Лебедев Максим
---
